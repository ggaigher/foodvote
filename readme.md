FOODVOTE - DBSERVER
==============

## COMENTÁRIOS PRINCIPAIS
1. `Preferi utilizar o sqlite in memory ao invés do in memory do EF para poder visualizar melhor a base e sua relação.`
2. `Não vi necessidade de aplicar o DDD por completo devido ser muito custoso para um projeto com regras tão simples.`
3. `Domínio anêmico`
4. `Classes totalmente coesas e com responsabilidade única`
5. `Testes unitários garantindo todos os critérios de aceitação`
6. `Foi utilizado o ORM Dapper (simples, poderoso e de simples manutenção, por ser bem próximo ao ADO, facilidade em montar as querys)`
7. `Implementado com autenticação e autorização via token JWT`
8. `Aplicação pronta para conteinerização via Docker (dockerfile)`


## O QUE MELHORIA?
1. `Não foi destacado como será a autenticação do profissional para o sistema.`
2. `Criar uma história que faria implementar uma identificação de usuário no sistema para saber (caso necessite) em qual restaurante o usuário votou`
3. `E se no caso de existir empate, não tem história para definir isso.`
5. `Qual é o critério de desempate caso tenha numero par de profissional na empresa?`
6. `Final de semana não tem votação`

## MIDDLEWARES CRIADOS
1. `Para log`
2. `Para tratar erros e dar o retorno correto de acordo com o statuscode (REST)`
3. `Para controlar transação com o banco de dados com base no request`
4. `Para autorização via token JWT`


## DEPENDENCIAS
1. `Dapper: ORM`
2. `Swashbukle: SWAGGER`
3. `Make: TASKRUNNER`
4. `SQLLite: BANCO DE DADOS LOCAL`
5. `Serilog: LOG`
6. `Unit: TESTES UNITÁRIOS`

