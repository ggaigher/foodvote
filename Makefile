publish:
	docker build -t $(REGISTRY_URI)/foodvote/api:$(GO_PIPELINE_LABEL) .
	docker push $(REGISTRY_URI)/foodvote/api:$(GO_PIPELINE_LABEL)

run: infra
	cd ./src/api/ && dotnet run 

test_unit:
	dotnet test ./src/tests.unit/

test_integrated: infra
	dotnet test ./src/tests.integration/
