FROM microsoft/aspnetcore-build:2.0 as build-environment
WORKDIR /app
COPY src .

RUN dotnet restore API.sln
RUN dotnet test -v q ./tests.unit/Tests.Unit.csproj
RUN dotnet publish ./api/API.csproj -c Release -o ./deploy-api

FROM microsoft/aspnetcore:2.0
WORKDIR /app
COPY --from=build-environment /app/api/deploy-api .
COPY --from=build-environment /app/wait-for-it.sh .  
ENTRYPOINT ["dotnet", "API.dll"]
