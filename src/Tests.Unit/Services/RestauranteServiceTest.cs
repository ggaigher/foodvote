﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Domain.Context;
using API.Domain.Helpers;
using API.Domain.Models;
using API.Domain.Models.ViewModels;
using API.Domain.Repositories;
using API.Domain.Services;
using API.Factories;
using FluentAssertions;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace Tests.Unit.Services
{
    public class RestauranteServiceTest
    {
        private RestauranteService _service;
        private Mock<ISqlContext> _sqlServiceMock = new Mock<ISqlContext>();
        private Mock<IConfiguration> _configuration = new Mock<IConfiguration>();
        private Mock<ILogFactory> _logFactory = new Mock<ILogFactory>();
        private Mock<IRestauranteRepository> _repository = new Mock<IRestauranteRepository>();
        private Mock<IDateTimeHelper> _dateTimeHelper = new Mock<IDateTimeHelper>();

        public RestauranteServiceTest()
        {
            _service = new RestauranteService(_repository.Object, _dateTimeHelper.Object);
            var moq = new AutoMoqCore.AutoMoqer();
        }

        [Fact(DisplayName = "Deve quebrar - um profissional só pode votar em um restaurante por dia")]
        public async Task DeveQuebrarVotarFaritoSoPodeVotarEmUmRestaurante()
        {
            _repository.Setup(x => x.BuscaVotacoesFiltradoAsync(It.IsAny<DateTime>(), null, It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(
             new List<Votacao>()
             {
                 new Votacao()
                 {
                     IdProfissional = 1,
                     IdRestaurante = 1,
                     Data = DateTime.Now.Date
                 },
                  new Votacao()
                 {
                     IdProfissional = 1,
                     IdRestaurante = 2,
                     Data = DateTime.Now.Date
                 },
             }.AsEnumerable());

            var votacao = new Votacao()
            {
                IdProfissional = 1,
                IdRestaurante = 1,
                Data = DateTime.Now.Date
            };

            var exception = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.VotarFavoritoAsync(votacao);
            });

            Assert.Equal("Um profissional só pode votar em um restaurante por dia.", exception.Message);
        }

        [Fact(DisplayName = "Deve quebrar - restaurante já foi votado essa semana.")]
        public async Task DeveQuebrarRestauranteJaFoiVotado()
        {
            _repository.Setup(x => x.BuscaVotacoesFiltradoAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(
             new List<Votacao>()
             {
                 new Votacao()
                 {
                     IdProfissional = 1,
                     IdRestaurante = 1,
                     Data = DateTime.Now.Date
                 },
                  new Votacao()
                 {
                     IdProfissional = 2,
                     IdRestaurante = 2,
                     Data = DateTime.Now.Date
                 }
             }.AsEnumerable());

            var votacao = new Votacao()
            {
                IdProfissional = 1,
                IdRestaurante = 1,
                Data = DateTime.Now.Date
            };

            var exception = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _service.VotarFavoritoAsync(votacao);
            });

            Assert.Equal("Restaurante já foi votado essa semana, favor escolher outro.", exception.Message);
        }

        [Fact(DisplayName = "Deve passar - resultado votacao.")]
        public async Task DevePassarResultadoVotacao()
        {
            _dateTimeHelper.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2019, 01, 11, 8, 40, 30));

            _repository.Setup(x => x.BuscaVotacoesFiltradoAsync(It.IsAny<DateTime>(), null, 0, 0)).ReturnsAsync(new List<Votacao>()
             {
                 new Votacao()
                 {
                     IdProfissional = 1,
                     IdRestaurante = 1, // HABIBS
                     Data = DateTime.Now.Date
                 },
                  new Votacao()
                 {
                     IdProfissional = 2,
                     IdRestaurante = 1, // HABIBS
                     Data = DateTime.Now.Date
                 },
                  new Votacao()
                 {
                     IdProfissional = 3,
                     IdRestaurante = 2, // SUBWAY
                     Data = DateTime.Now.Date
                 }
             });

            _repository.Setup(x => x.BuscaRestaurantePorIdAsync(It.IsAny<int>())).ReturnsAsync(new Restaurante()
            {
                Id = 1,
                Localizacao = "Rua do teste, 681",
                Nome = "HABIBS",
                Votacoes = new List<Votacao>()
                 {
                     new Votacao()
                     {
                         IdProfissional = 1,
                         IdRestaurante = 1, // HABIBS
                         Data = DateTime.Now.Date
                     },
                      new Votacao()
                     {
                         IdProfissional = 2,
                         IdRestaurante = 1, // HABIBS
                         Data = DateTime.Now.Date
                     }
                 }
            });

            var resultado = await _service.ResultadoVotacaoDiaAsync();

            var resultadoEsperado = new ResultadoVotacao()
            {
                QtdeVotacoes = 2,
                Restaurante = "HABIBS"
            };

            resultado.ShouldBeEquivalentTo(resultadoEsperado);
        }

        [Fact(DisplayName = "Deve passar - votação realizada com sucesso.")]
        public async Task DevePassarRestauranteVotadoComSucesso()
        {
            var votacao = new Votacao()
            {
                IdProfissional = 1,
                IdRestaurante = 1,
                Data = DateTime.Now.Date
            };

            await _service.VotarFavoritoAsync(votacao);

            _repository.Verify(x => x.Insere(votacao.IdProfissional, votacao.IdRestaurante));
        }

        [Fact(DisplayName = "Deve quebrar - votação realizada após horário permitido.")]
        public async Task DeveQuebrarRestauranteVotadoAposHorario()
        {
            _dateTimeHelper.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2019, 01, 11, 12, 40, 30));

            var exception = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                 await _service.ResultadoVotacaoDiaAsync();
            });

            Assert.Equal("Votação encerrada! Verifique o resultado.", exception.Message);
        }
    }
}
