﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace API.Factories
{
    public interface IConnectionFactory
    {
        IDbConnection Connection();

        Task OpenConnectionAsync();

        void OpenConnection();

        void CloseConnection();

        IDbTransaction BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();
    }

    public class ConnectionFactory 
    {
        private readonly SQLiteConnection _connection;
        private SQLiteTransaction _transaction;
        private bool _isTransactionOpen;

        public ConnectionFactory(IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("ConnectionStrings:SqlLite");

            _connection = new SQLiteConnection(connectionString.Value);
        }
        public IDbConnection Connection()
        {
            SqlMapper.AddTypeMap(typeof(bool), DbType.Int32);
            return _connection;
        }
        public IDbTransaction BeginTransaction()
        {
            if (_transaction == null)
            {
                if (_connection.State != ConnectionState.Open)
                    throw new Exception("A conexão com o banco não esta aberta.");

                _transaction = _connection.BeginTransaction();
            }

            _isTransactionOpen = true;

            return _transaction;
        }

        public void CommitTransaction()
        {
            if (_isTransactionOpen)
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }

            _isTransactionOpen = false;
        }
        public void RollbackTransaction()
        {
            if (_isTransactionOpen)
                _transaction.Rollback();
        }
        public async Task OpenConnectionAsync()
        {
            var connection = _connection as SQLiteConnection;

            await connection.OpenAsync();
        }

        public void OpenConnection()
        {
            var connection = _connection as SQLiteConnection;

            _connection.Open();
        }

        public void CloseConnection()
        {
            _connection.Close();

            _connection.Dispose();
        }
    }
}
