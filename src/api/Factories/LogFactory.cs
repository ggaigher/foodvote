﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using System;

namespace API.Factories
{
    public interface ILogFactory
    {
        ILogger Logger();
    }

    public class LogFactory
    {
        private readonly ILogger _logger;
        private readonly string aditionalColumn = "RequestId";

        public LogFactory(IConfiguration configuration, IRequestIdFactory requestIdFactory) => _logger = ConfigureLogger(configuration, requestIdFactory);

        public ILogger Logger() => _logger;

        private ILogger ConfigureLogger(IConfiguration configuration, IRequestIdFactory requestIdFactory)
        {
            var loggerConfiguration = new LoggerConfiguration();
            loggerConfiguration
                .MinimumLevel.Is((LogEventLevel)Convert.ToInt32(configuration.GetSection("Logging:Serilog:LogLevel")?.Value))
                .Enrich.FromLogContext()
                .Enrich.WithProperty(aditionalColumn, requestIdFactory.Id());

            ConfigureRollingFileSink(loggerConfiguration);

            return loggerConfiguration.CreateLogger();
        }

        private void ConfigureRollingFileSink(LoggerConfiguration loggerConfiguration)
        {
            const string output = "[{Timestamp:dd-MM-yyyy HH:mm:ss}] [{Level}] [{RequestId}] {Message}{NewLine}{Exception}";

            loggerConfiguration.WriteTo.Console(outputTemplate: output)
#if DEBUG
                .WriteTo.RollingFile("Logs/log-{Date}.txt", shared: true)
#endif
            ;
        }
    }
}
