﻿using API.Factories;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace API.Middlewares
{
    public class TransactionMiddleware
    {
        private readonly RequestDelegate _next;

        public TransactionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IConnectionFactory connectionFactory)
        {
            await connectionFactory.OpenConnectionAsync();

            connectionFactory.BeginTransaction();
            
            try
            {
                await _next(context);

                connectionFactory.CommitTransaction();
            }
            catch
            {
                connectionFactory.RollbackTransaction();

                throw;
            }
            finally
            {
                connectionFactory.CloseConnection();
            }
        }
    }
}