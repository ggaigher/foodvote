﻿using API.Factories;
using Microsoft.AspNetCore.Http;
using Serilog.Events;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace API.Middlewares
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILogFactory logFactory)
        {
            var stopWatch = Stopwatch.StartNew();

            if (context.Request.ContentType != null && !context.Request.ContentType.Contains("multipart/form-data"))
                logFactory.Logger().Information($"REQUEST | {context.Request.Method} > {context.Request.Path} > { await FormatRequest(context.Request) }");

            var originalBodyStream = context.Response.Body;

            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                await _next(context);

                logFactory.Logger().Information($"RESPONSE | {context.Request.Method} > {context.Request.Path} > { await FormatResponse(context.Response) }");

                await responseBody.CopyToAsync(originalBodyStream);
            }

            stopWatch.Stop();

            var statusCode = context.Response?.StatusCode;

            var level = stopWatch.Elapsed.TotalSeconds > 5 ? LogEventLevel.Error : LogEventLevel.Information;

            logFactory.Logger().Write(level, $"DURATION | {context.Request.Method} > {context.Request.Path} > {statusCode} em {stopWatch.Elapsed.TotalMilliseconds:0.0000} ms");
        }

        private async Task<string> FormatRequest(HttpRequest httpRequest)
        {
            var requestStream = new MemoryStream();

            await httpRequest.Body.CopyToAsync(requestStream);

            requestStream.Seek(0, SeekOrigin.Begin);

            var request = await new StreamReader(requestStream).ReadToEndAsync();

            requestStream.Seek(0, SeekOrigin.Begin);

            httpRequest.Body = requestStream;

            return request;
        }

        private async Task<string> FormatResponse(HttpResponse httpResponse)
        {
            httpResponse.Body.Seek(0, SeekOrigin.Begin);

            var response = await new StreamReader(httpResponse.Body).ReadToEndAsync();

            httpResponse.Body.Seek(0, SeekOrigin.Begin);

            return response;
        }
    }
}