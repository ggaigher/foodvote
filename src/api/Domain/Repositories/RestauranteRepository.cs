﻿using API.Domain.Context;
using API.Domain.Models;
using API.Domain.Models.ViewModels;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Domain.Repositories
{
    public class RestauranteRepository : IRestauranteRepository
    {
        private readonly ISqlContext _sqlContext;

        public RestauranteRepository(ISqlContext sqlContext)
        {
            _sqlContext = sqlContext;
        }

        public async Task<Restaurante> BuscaRestaurantePorIdAsync(int id)
        {
            var sql = @"
                      SELECT * FROM R.ID,
                             R.NOME,
                             R.LOCALIZACAO
	                    FROM RESTAURANTE R INNER JOIN VOTACAO V
                        WHERE ID = :id
            ";

            var command = new CommandDefinition(sql);

            var retorno = await _sqlContext
                            .GetConnection()
                            .QueryAsync<Restaurante, Votacao, Restaurante>(command,
                                (restaurante, votacao) =>
                                {
                                    if (restaurante.Votacoes == null)
                                        restaurante.Votacoes = new List<Votacao>();

                                    restaurante.Votacoes.Add(votacao);

                                    return restaurante;
                                },
                                "Id,IdRestaurante"
                            );

            return retorno.FirstOrDefault();
        }


        public async Task<IEnumerable<Votacao>> BuscaVotacoesFiltradoAsync(DateTime dtInicio,
        DateTime? dtFim,
        int idRestaurante,
        int idProfissional)
        {
            var sql = $@"
                        SELECT
                            V.IDRESTAURANTE,
                            V.IDPROFISSIONAL
                        FROM
                            VOTACAO V
                            {Filtro(dtInicio, dtFim, idRestaurante, idProfissional)}
                        ";

            var command = new CommandDefinition(sql);

            return await _sqlContext.QueryAsync<Votacao>(sql, new
            {
                IDPROFISSIONAL = idProfissional,
                IDRESTAURANTE = idRestaurante,
                DATA = DateTime.Now.Date
            });
        }

        public async Task Insere(int idProfissional, int idRestaurante)
        {
            var sql = @"
                INSERT INTO VOTACAO(
                                   IDPROFISSIONAL,
                                   IDRESTAURANTE)
                     VALUES (
                             :IDPROFISSIONAL,
                             :IDRESTAURANTE)
            ";

            await _sqlContext.ExecuteAsync(sql, new
            {
                IDPROFISSIONAL = idProfissional,
                IDRESTAURANTE = idRestaurante,
                DATA = DateTime.Now.Date
            });
        }

        private string Filtro(DateTime dtInicio,
          DateTime? dtFim,
          int idRestaurante,
          int idProfissional)
        {
            var parametros = new StringBuilder();
            string Prefixo() => (parametros.Length == 0 ? "WHERE" : "AND");
            
            if (idRestaurante > 0)
                parametros.Append($"{Prefixo()} IDRESTAURANTE = {idRestaurante}");

            if (idProfissional > 0)
                parametros.Append($"{Prefixo()} IDPROFISSIONAL = {idProfissional}");

            if (!dtFim.HasValue)
                parametros.Append($"{Prefixo()} DATA = '{dtInicio.Date}'");
            else
                parametros.Append($"{Prefixo()} DATA BETWEEN '{dtInicio.Date}' AND '{dtFim.Value.Date}'");

            return parametros.ToString();
        }
    }
}
