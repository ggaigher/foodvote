﻿using API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Domain.Repositories
{
    public interface IRestauranteRepository
    {
        Task<Restaurante> BuscaRestaurantePorIdAsync(int id);
        Task<IEnumerable<Votacao>> BuscaVotacoesFiltradoAsync(DateTime dtInicio,
          DateTime? dtFim,
          int idRestaurante,
          int idProfissional);
        Task Insere(int idProfissional, int idRestaurante);
    }
}
