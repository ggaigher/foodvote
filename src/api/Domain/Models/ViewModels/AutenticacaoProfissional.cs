﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Domain.Models.ViewModels
{
    public class AutenticacaoProfissional
    {
        public string Login { get; set; }
        public string Senha { get; set; }
    }
}
