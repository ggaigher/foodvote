﻿using System;

namespace API.Domain.Models
{
    public class Votacao
    {
        //public Restaurante Restaurante { get; set; }
        //public Profissional Profissional { get; set; }
        public int IdRestaurante { get; set; }
        public int IdProfissional { get; set; }
        public DateTime Data { get; set; }
    }
}
