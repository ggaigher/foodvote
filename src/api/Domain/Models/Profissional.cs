﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Domain.Models
{
    public class Profissional
    {
        public int Id;
        public string Nome;
        public string Setor;
        public List<Votacao> Votacoes { get; set; }
    }
}
