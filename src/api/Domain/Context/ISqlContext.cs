﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace API.Domain.Context
{
    public interface ISqlContext
    {
        Task<T> ExecuteScalarAsync<T>(string sql, object param);
        Task ExecuteAsync(string sql, object param);
        Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null);
        Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param = null);
        Task<IDataReader> ExecuteReader(string sql, object param);
        Task<int> ExecuteIdentity(string sql, string identityColumn, DynamicParameters param);
        IDbConnection GetConnection();
    }
}
