﻿using API.Factories;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace API.Domain.Context
{
    public class SqlContext : ISqlContext
    {
        private readonly IConnectionFactory _connectionFactory;

        public SqlContext(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<T> ExecuteScalarAsync<T>(string sql, object param)
        {
            var transaction = _connectionFactory.BeginTransaction();

            var command = new CommandDefinition(sql, param, transaction);

            return await _connectionFactory.Connection().ExecuteScalarAsync<T>(command);
        }

        public async Task ExecuteAsync(string sql, object param)
        {
            var transaction = _connectionFactory.BeginTransaction();

            var command = new CommandDefinition(sql, param, transaction);

            await _connectionFactory.Connection().ExecuteAsync(command);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null)
        {
            var transaction = _connectionFactory.BeginTransaction();

            var command = new CommandDefinition(sql, param, transaction);

            return await _connectionFactory.Connection().QueryAsync<T>(command);
        }

        public async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param = null)
        {
            var transaction = _connectionFactory.BeginTransaction();

            var command = new CommandDefinition(sql, param, transaction);

            return await _connectionFactory.Connection().QueryFirstOrDefaultAsync<T>(command);
        }

        public async Task<IDataReader> ExecuteReader(string sql, object param)
        {
            var transaction = _connectionFactory.BeginTransaction();

            var command = new CommandDefinition(sql, param, transaction);

            return await _connectionFactory.Connection().ExecuteReaderAsync(command);
        }

        public async Task<int> ExecuteIdentity(string sql, string identityColumn, DynamicParameters param)
        {
            var transaction = _connectionFactory.BeginTransaction();

            await _connectionFactory.Connection().ExecuteAsync(sql, param, transaction);

            return param.Get<int>(identityColumn);
        }

        public IDbConnection GetConnection() => _connectionFactory.Connection();
    }
}
