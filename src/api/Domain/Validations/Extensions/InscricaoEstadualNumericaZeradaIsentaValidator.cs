﻿using FluentValidation.Validators;

namespace API.Domain.Validations.Extensions
{
    public class InscricaoEstadualNumericaZeradaIsentaValidator : PropertyValidator
    {
        public InscricaoEstadualNumericaZeradaIsentaValidator() : base("{PropertyName} não é válido.")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var texto = context.PropertyValue as string;

            if (!string.IsNullOrWhiteSpace(texto))
            {
                return (long.TryParse(texto, out long valor) && valor > 0) || texto.ToUpper() == "ISENTA";
            }

            return false;
        }
    }
}