﻿using FluentValidation.Validators;
using System;

namespace API.Domain.Validations.Extensions
{
    public class DataNascimentoValidator : PropertyValidator
    {
        public DataNascimentoValidator() : base("{PropertyName} não é válido.")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var data = (DateTime)context.PropertyValue;
            return (data >= new DateTime(1900, 1, 1) && data <= DateTime.Now);
        }
    }
}
