﻿using FluentValidation;

namespace API.Domain.Validations.Extensions
{
    public static class Extensions
    {
        public static IRuleBuilderOptions<T, TElement> ValidCpf<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new CpfValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidCnpj<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new CnpjValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidEmail<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new EmailValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidDataNascimento<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new DataNascimentoValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidSomenteNumeros<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new SomenteNumerosValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidarInscricaoEstadualNumericaOuIsenta<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new InscricaoEstadualNumericaIsentaValidator());
        }

        public static IRuleBuilderOptions<T, TElement> ValidarInscricaoEstadualNumericaZeradaOuIsenta<T, TElement>(this IRuleBuilder<T, TElement> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new InscricaoEstadualNumericaZeradaIsentaValidator());
        }
    }
}
