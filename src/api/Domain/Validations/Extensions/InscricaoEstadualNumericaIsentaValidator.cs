﻿using FluentValidation.Validators;

namespace API.Domain.Validations.Extensions
{
    public class InscricaoEstadualNumericaIsentaValidator : PropertyValidator
    {
        public InscricaoEstadualNumericaIsentaValidator() : base("{PropertyName} não é válido.")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var texto = context.PropertyValue as string;

            if (!string.IsNullOrWhiteSpace(texto))
            {
                return long.TryParse(texto, out long valor) || texto.ToUpper() == "ISENTA";
            }

            return false;
        }
    }
}
