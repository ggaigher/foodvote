﻿using FluentValidation.Validators;
using System.Text.RegularExpressions;

namespace API.Domain.Validations.Extensions
{
    public class EmailValidator : PropertyValidator
    {
        public EmailValidator() : base("{PropertyName} não é válido.")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var email = context.PropertyValue as string;

            if (!string.IsNullOrWhiteSpace(email))
            {
                var match = Regex.Match(email, "[\\w-]+@([\\w-]+\\.)+[\\w-]+");

                return match.Success;
            }

            return false;
        }
    }
}
