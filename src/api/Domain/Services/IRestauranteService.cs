﻿using API.Domain.Models;
using API.Domain.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Domain.Services
{
    public interface IRestauranteService
    {
        Task CadastrarAsync(Restaurante restaurante);
        Task ListarAsync();
        Task VotarFavoritoAsync(Votacao votacao);
        Task<ResultadoVotacao> ResultadoVotacaoDiaAsync();
    }
}
