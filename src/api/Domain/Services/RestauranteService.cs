﻿using API.Domain.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using API.Extensions;
using API.Domain.Repositories;
using API.Domain.Models.ViewModels;
using API.Domain.Helpers;

namespace API.Domain.Services
{

    public class RestauranteService : IRestauranteService
    {
        private readonly IRestauranteRepository _restauranteRepository;
        private IDateTimeHelper _dateTimeHelper;

        public RestauranteService(IRestauranteRepository restauranteRepository, IDateTimeHelper dateTimeHelper)
        {
            _restauranteRepository = restauranteRepository;
            _dateTimeHelper = dateTimeHelper;
        }

        public Task CadastrarAsync(Restaurante restaurante)
        {
            throw new NotImplementedException();
        }

        public Task ListarAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<ResultadoVotacao> ResultadoVotacaoDiaAsync()
        {
            var dataAtual = _dateTimeHelper.GetDateTimeNow();
            if (dataAtual.TimeOfDay > TimeSpan.Parse("11:59:00"))
            {
                throw new ValidationException("Votação encerrada! Verifique o resultado.");
            }

            var votacoes = await _restauranteRepository.BuscaVotacoesFiltradoAsync(dataAtual, null, 0, 0);

            var idRestauranteMaisVotado = votacoes.Select(x => x.IdRestaurante).Distinct().OrderByDescending(x => x).FirstOrDefault();

            var restaurante = await _restauranteRepository.BuscaRestaurantePorIdAsync(idRestauranteMaisVotado);

            var retornoResultadoVotacao = new ResultadoVotacao()
            {
                Restaurante = restaurante.Nome,
                QtdeVotacoes = restaurante.Votacoes.Count()
            };

            return retornoResultadoVotacao;
        }

        public async Task VotarFavoritoAsync(Votacao votacao)
        {
            if(votacao == null)
                throw new ArgumentNullException(nameof(votacao));

            if (votacao.IdProfissional < 1)
                throw new ArgumentOutOfRangeException($"Profissional não encontrado.");

            if (votacao.IdRestaurante < 1)
                throw new ArgumentOutOfRangeException($"Restaurante não encontrado.");

            var votacaoPorProfissional = await BuscaVotacaoPorProfissionalRestaurante(votacao.IdProfissional, votacao.IdRestaurante);

            if(votacaoPorProfissional != null)
            {
                throw new ValidationException("Um profissional só pode votar em um restaurante por dia.");
            }

            if(RestauranteJaVotadoNaSemana(votacao.IdRestaurante).Result)
            {
                throw new ValidationException("Restaurante já foi votado essa semana, favor escolher outro.");
            }

            await _restauranteRepository.Insere(votacao.IdRestaurante, votacao.IdProfissional);
        }

        private async Task<Votacao> BuscaVotacaoPorProfissionalRestaurante(int idProfissional, int idRestaurante)
        {
            var votacoes = await _restauranteRepository.BuscaVotacoesFiltradoAsync(DateTime.Now.Date, null, idRestaurante, idProfissional);

            return votacoes.FirstOrDefault();
        }

        private async Task<bool> RestauranteJaVotadoNaSemana(int idRestaurante)
        {
            var primeiroDiaDaSemana = DateTime.Now.StartOfWeek(DayOfWeek.Monday);

            var votacoes = await _restauranteRepository.BuscaVotacoesFiltradoAsync(primeiroDiaDaSemana, DateTime.Now.Date, idRestaurante, 0);

            return votacoes.Any();
        }
    }
}
