﻿using API.Domain.Models;
using API.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class RestauranteController : Controller
    {
        private readonly IRestauranteService _restauranteService;

        public RestauranteController(IRestauranteService restauranteService)
        {
            _restauranteService = restauranteService;
        }

        [HttpGet("api/foodvote/resultado-votacao")]
        public async Task<IActionResult> ListResultadoVotacao()
        {
            var votacao = await _restauranteService.ResultadoVotacaoDiaAsync();

            return Ok(votacao);
        }

        [HttpPost("api/foodvote/votar-favorito")]
        public async Task<IActionResult> Post([FromBody]int idRestaurante)
        {
            var usuario = User.Claims.FirstOrDefault(x => x.Type == "userId");

            var votacao = new Votacao()
            {
                IdRestaurante = idRestaurante,
                IdProfissional = Convert.ToInt32(usuario.Value)
            };

            await _restauranteService.VotarFavoritoAsync(votacao);

            return Ok(votacao);
        }
    }
}
